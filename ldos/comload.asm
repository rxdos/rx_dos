
%if 0

lDOS COMLOADER - load embedded programs
 by C. Masloch, 2018

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


%include "lmacros2.mac"
%include "lstruct.mac"


%define FOLLOWS INIT3
%define ENDS none
%define ADDRESS 0
%define COMMAND_LIST none, "", ""
	%imacro nextcomprogram 2-4+.nolist "",vstart=256
	align 16
%[ENDS]_end:
section %1 follows=%[FOLLOWS] %4
%1_start:
%1_position equ ADDRESS
 %define FOLLOWS %1
 %define ENDS %1
 %xdefine ADDRESS (ADDRESS + (%1_end - %1_start))
 %xdefine COMMAND_LIST COMMAND_LIST, %1, %2, %3
	%endmacro

	struc COMPROGRAMLIST_ENTRY
cplPositionSeg:	resw 1
cplLengthWords:	resw 1
cplNameString:	resw 1
cplHelpString:	resw 1
	endstruc

	%imacro comprogramlist 0-*
%if (%0 % 3) != 0 || %0 < 6
 %error Expected number of parameters a multiple of 3 and at least 6
%endif
%rep (%0 - 6) / 3
%rotate 3
	istruc COMPROGRAMLIST_ENTRY
at cplPositionSeg,	dw %1_position >> 4
at cplLengthWords,	dw (%1_end - %1_start) >> 1
at cplNameString,	dw msg_name.%1
at cplHelpString,	dw msg_help.%1
	iend
%endrep
	%endmacro

	%imacro comprogrammessages 0-*
%if (%0 % 3) != 0 || %0 < 6
 %error Expected number of parameters a multiple of 3 and at least 6
%endif
%rep (%0 - 6) / 3
%rotate 3
msg_name.%1:	asciz %2
msg_help.%1:	asciz %3
%endrep
	%endmacro


nextcomprogram COMINT3, "INT3", "program that breaks to a debugger"
	mov dx, .msg
	mov ah, 09h
	int 21h
	int3
	mov ax, 4C00h
	int 21h

.msg:	db "Embedded executable test loaded.",13,10,36


nextcomprogram COMEMPTY, "EMPTY", "empty program for testing"
	mov ax, 4C00h
	int 21h


nextcomprogram COMLIST, "LIST", "lists all available programs"
	mov dx, .msg
	mov ah, 09h
	int 21h

	mov ds, cx		; ds:bx -> command program name table
	mov es, cx

	mov di, bx		; es:di = ds:di -> table
.loop:
	cmp word [di + cplLengthWords], 0
	je .end
	mov dx, word [di + cplNameString]
	 push di
	mov di, dx
	mov al, 0
	mov cx, -1
	repne scasb
	neg cx
	dec cx
	dec cx
	mov ah, 40h
	mov bx, 1
	int 21h

	cmp cx, 8
	mov cx, 1
	jae @F
	inc cx
@@:
	mov ah, 02h
	mov dl, 9
	int 21h
	loop @B

	 pop di
	 mov dx, word [di + cplHelpString]
	 push di

	mov di, dx
	mov al, 0
	mov cx, -1
	repne scasb
	neg cx
	dec cx
	dec cx
	mov ah, 40h
	mov bx, 1
	int 21h

	mov ah, 02h
	mov dl, 13
	int 21h
	mov ah, 02h
	mov dl, 10
	int 21h

	 pop di
	add di, COMPROGRAMLIST_ENTRY_size
	jmp .loop

.end:
	mov ax, 4C00h
	int 21h

.msg:
	db "Command program names:",13,10,36


nextcomprogram COMINSTSECT, "INSTSECT", "installs boot sectors to drives"
	incbin "instsect.com"


nextcomprogram COMVERSION, "VERSION", "lists the version of this kernel file"
	mov di, .msg
	mov dx, di	; -> message
	mov al, 0
	mov cx, -1
	repne scasb
	neg cx
	dec cx
	dec cx		; strlen
	mov ah, 40h
	mov bx, 1
	int 21h

	mov ax, 4C00h
	int 21h

.msg:
			db RXDOSLONG_S, " version ", RXDOSVERSIONLONG_S
%ifnidn _REVISIONID, ""
			db " [",_REVISIONID,"]"
%endif
			db " [",__DATE__,"]"
			asciz 13,10


nextcomprogram COMLOADER, "", "", vstart=0
		; INP:	ds = es = PSP
		;	cs:ip = COMLOADER segment : 0
	push cs
	pop ds
	mov dx, .msg		; -> .msg
	mov ah, 09h
	int 21h			; display
	int3

	push es
	pop ds	
	mov si, 81h		; PSP command line
@@:
	lodsb
	cmp al, 9
	je @B
	cmp al, 32
	je @B			; skip leading spaces -->
	jb .error_no_name_given	; end of line ? -->

	mov di, si
	dec di			; -> name

	db __TEST_IMM8
@@:
	lodsb
	cmp al, 'a'
	jb @F
	cmp al, 'z'
	ja @F
	sub al, 'a' - 'A'
	mov byte [si - 1], al
@@:
	cmp al, 32
	ja @BB
	mov byte [si - 1], 0	; ! al preserved here until after .got_it

	mov cx, si
	sub cx, di

	push cs
	pop ds
	mov si, .programs

.loop:
	cmp word [si + cplLengthWords], 0	; empty payload ?
	je .error_unknown_name_given		; yes, end of table -->

	push cx
	push si
	push di
	mov si, word [si + cplNameString]
	repe cmpsb		; this command ?
	pop di
	pop si
	pop cx
	je .got_it

	add si, COMPROGRAMLIST_ENTRY_size
	jmp .loop

.got_it:
	mov bx, word [si + cplPositionSeg]
				; address (paragraphs from after INIT3 end MCB)
	push word [si + cplLengthWords]
				; number of words

	 push ax
	 push bx
	mov dx, .msg_loading_before
	mov ah, 09h
	int 21h

	dec cx
	mov dx, word [si + cplNameString]
	mov ah, 40h
	mov bx, 1
	int 21h

	mov dx, .msg_loading_after
	mov ah, 09h
	int 21h

	push es
	pop ds			; => PSP

	add di, cx
	mov si, di
	 pop bx
	 pop ax
	mov byte [si], al	; ! preserved from before .loop

	mov di, 81h
	mov cx, 7Fh
	rep movsb		; move down the command line trail

	mov di, 81h
	mov cx, 7Fh
	mov al, 13
	repne scasb
; COUNTER ->13, cx=7Fh, di=81h
; scasb
; COUNTER 13 ->, ZR, cx=7Eh, di=82h
	neg cx
; cx=-7Eh
	add cx, 7Eh
; cx=0

; COUNTER ->32, 13, cx=7Fh, di=81h
; scasb
; COUNTER 32 ->13, NZ, cx=7Eh, di=82h
; scasb
; COUNTER 32 13 ->, ZR, cx=7Dh, di=83h
	; neg cx
; cx=-7Dh
	; add cx, 7Eh
; cx=1
	mov byte [80h], cl	; update command line trail length

	mov di, 256
	pop cx			; number of words
	mov ax, cs
	sub ax, (COMLOADER_position >> 4)
	add ax, bx		; address
	mov ds, ax
	xor si, si		; ds:si -> source
	push di
	rep movsw		; move to behind PSP

	mov di, es
	add di, 1000h		; => behind area to be used by stack
	mov ax, cs		; => COMLOADER segment
	cmp ax, di		; COMLOADER starts above-or-equal stack end ?
	jae @F

	mov dx, .msg_internal_error_memory
	jmp .error_exit

@@:
	pop di

	push es
	cli
	pop ss
	xor sp, sp		; full 64 KiB's stack
	sti
	xor ax, ax		; ax = 0 (expected not to be used)
	push ax			; 0 word on stack

	 push es
	 pop ds			; ds = es = ss = cs = PSP

	mov cx, cs
	mov bx, .programs	; cx:bx -> command program name table
				; (used by the associated COMLIST program)

	push es
	push di			; -> PSP : 256
	retf


.error_unknown_name_given:
	mov dx, .msg_unknown_name_given
	jmp .error_exit

.error_no_name_given:
	mov dx, .msg_no_name_given
.error_exit:
	push cs
	pop ds
	mov ah, 09h
	int 21h
	mov ax, 4C01h
	int 21h


.msg:	db RXDOSLONG_S," COMLOADER",36
.msg_loading_before:
	db " - loading command program ",36
.msg_loading_after:
	db 13,10,36
.msg_no_name_given:
	db 13,10,"No command program name specified!",13,10,36
.msg_unknown_name_given:
	db 13,10,"Specified command program name not found!",13,10,36
.msg_internal_error_memory:
	db 13,10,"Internal error, lacking memory for nonrelocated stack."
	db 13,10,36

	align 8
.programs:
	comprogramlist COMMAND_LIST
	dw 0, 0

	comprogrammessages COMMAND_LIST


nextcomprogram COMNONE, "", ""

