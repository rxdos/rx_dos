
%if 0

lDOS interrupt 2Fh handling
 by C. Masloch, 2018

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

		; Interrupt 2F
		;
		; DOS hard-coded multiplex, MS Windows, many services
relocated i2F:
	cmp ax, 4A04h
	je get_first_hmcb
	cmp ah, 12h
	je i2F_12
	iret


get_first_hmcb:
	push dx
	push ds
	mov dx, 70h
	mov ds, dx
	mov ds, word [dosentry_to_dosdata_segment]
	mov dx, word [first_hmcb]
		; Note:	In MS-DOS 7.10, if DOS is not in the HMA, this
		;	 points at eg 021Ch:0030h, which seems to be an
		;	 LMA DOSCODE segment (BIO?). That HMCB has its
		;	 hmcbSignature field set to "MS", but hmcbOwner,
		;	 hmcbSize, and hmcbNext are all zero.
		;	We instead only support the call if in the HMA.
	test dx, dx
	jz .return

	xor ax, ax
	dec ax
	mov es, ax
	inc ax
	mov di, dx
.return:
	pop ds
	pop dx
	iret


iret_CY_ax1:
	mov ax, 1		; error 1, invalid function
	stc			; CY
iret_CF:
	lframe 0
	lpar word, flags
	lpar dword, csip
	lenter
	rcr byte [bp + ?flags], 1	; rotate CF (error state) into bit7 of saved FL
	rol byte [bp + ?flags], 1	; rotate error state into bit0+CF
	lleave
i2F_iret:
	iret


iret_ZF:
	lframe 0
	lpar word, flags
	lpar dword, csip
	lenter
	jz .set_ZR
.set_NZ:
	and byte [bp + ?flags], ~40h
	jmp .ret

.set_ZR:
	or byte [bp + ?flags], 40h
.ret:
	lleave
	iret


	align 2
i2F_12_functions:
.:
	dw i2F_12_00
	dw i2F_12_01
	dw i2F_12_02
	dw i2F_12_03
	dw i2F_12_04
	dw i2F_12_05
	dw i2F_12_06
.amount: equ ($-.) / 2


		; Internal DOS services. Some of these may need ss = DOSDATA.
i2F_12:
	cmp al, i2F_12_functions.amount
	jae i2F_iret
	push bx
	push bp
	mov bp, sp
	xor bx, bx
	mov bl, al
	add bx, bx
	mov bx, [cs:i2F_12_functions + bx]
	xchg word [bp + 2], bx
	pop bp
	retn


i2F_12_00:
%if 0		; Do not set this yet, only when most subfunctions implemented.
	mov al, -1
%endif
	iret


i2F_12_01:
	les di, [ss:SDApCurrentSFT]
	push dx
	call _SFTCloseFile		; close SFT Entry
	pop dx
	mov cx, word [es:di + sftRefCount]
	jmp iret_CF


i2F_12_02:
	lframe int
	lpar	word, vector
	lpar_return
	lenter
	xor bx, bx
	mov es, bx
	mov bl, [bp + ?vector]
	add bx, bx
	add bx, bx
	lleave
	lret


i2F_12_03:
	push ax
	mov ax, 70h
	mov ds, ax
	pop ax
	mov ds, word [dosentry_to_dosdata_segment]
	iret


i2F_12_04:
	lframe int
	lpar	word, char
	lpar_return
	lenter
	mov al, byte [bp + ?char]
	cmp al, '/'
	jne @F
	mov al, '\'
@@:
	cmp al, '\'
	lleave
	jmp iret_ZF


i2F_12_05:
	lframe int
	lpar	word, char
	lpar_return
	lenter
	mov dl, byte [bp + ?char]
	call _DisplayOutput
	lleave
	lret


i2F_12_06:
	lframe int
	lpar	word, ax
	lpar_return
	lenter
	push si
	push bx
	push ax
	or byte [ss:error_flags], EF_2F1206
	mov si, i2F_doscode_msg.2F1206
	call dos_disp_msg
	pop ax
	pop bx
	pop si
	mov al, 3			; fail
	lleave
	lret


i2F_doscode_msg:
.2F1206:	asciz "Int2F.1206 called!",13,10

