
iniblz is a BriefLZ depacker stage for lDOS's (and RxDOS's) iniload kernel.

For usage conditions, refer to comments in iniblz.asm


Example usage:

iniblz$ blzpack /path/to/rxdos-7.2x/ldos/lDOSLOAD.BIN lDOSLOAD.BLZ
iniblz$ ./mak.sh -I/path/to/lmacros/
rxdos-7.2x/ldos$ nasm -Ox -D_REVISIONID="'hg $(hg -R .. id -i)'" \
 -I ../lmacros/ -f bin -o ../bin/RxDOS.COM -l ../lst/RxDOS.LST \
 -D_MAP=../map/RxDOS.MAP ../ldosboot/iniload.asm \
 -D_PAYLOAD_FILE=/path/to/iniblz/iniblz.bin


Currently, the system must have enough memory to hold all of the following:

* 20 KiB of buffers (8 KiB FAT sector, 8 KiB DMA boundary buffer) and stack
  and boot sector, at the top of the LMA (below RPL or EBDA or video memory).

* The depacker's INIT1 section, which contains the actual BriefLZ depacker
  and a bit of handling around it. This is directly below the "load top",
  which gives the segment of the lowest buffer at the LMA top.
  (INIT0 and INIT1 together are smaller than 2 KiB currently. As iniload's
  loader is 2 KiB, this means the uncompressed part is less than 4 KiB.)

* The compressed image.

* The decompressed image. This is placed at linear 600h currently.

Note:	The compressed image and decompressed image may now overlap.
	 To succeed, the write pointer has to stay below-or-equal the
	 read pointer. Otherwise the decompression errors out.


Possibilities:

* init0_movp can always move DN because the payload-and-INIT1 destination is
  always above-or-equal the source of that move. (done: _OPTIMISE_MOVP_DN_ONLY)

* The depacker follows the C source rather closely, which is not necessary.
  (somewhat done)

* The depacker could support overlapping source and destination buffers,
  in which case it should check that the destination write pointer stays
  below the source read pointer. As the kernel is typically below 128 KiB
  in size, this is only needed for systems with 256 KiB or less of LMA.
  (done: _ALLOW_OVERLAPPING)

* The safe C depacker was used as base. The additional checks could be dropped.

* _IMAGE_EXE could be supported, with some adjustments to the segmentation
  to allow the same image to be entered either as kernel or as MZ executable.

* Optimisations are possible if the block size is decreased, however,
  I prefer it to support any block size, which it does for now.

* CRC checks of compressed or decompressed data could be added.

* If a kernel CONFIG section is added to iniload or lDOS init, iniblz
  should also support this. (Embed one in the uncompressed header, pass it on
  to the kernel when loading.)

* Handling and structures could be added to allow a to-be-written utility
  to compress and decompress images within existing kernel files. However,
  typically the compressing during building the kernel is sufficient.
  (The FreeDOS kernel and EDR-DOS's also do not have such utilities.)

* Other compression algorithms could be used.

* If the depacker stage was integrated into iniload, some common code could
  be shared, mainly the error handling.

