Changes of RxDOS version 7.20N since 7.1.5
---------------------------------------------

8.1 Initial changes, individual programs' fates

     1. Only Netwide Assembler (NASM) 2.13.02 used for assembling.

     2. Changed names of these programs:

          RxDOSCMD   to RxCMD

     3. Dropped some programs:

          RxDOSBIO    (renamed RxBIO, later integrated into RxDOS.COM)
          RxD_BOOT    (dropped, use ldosboot and instsect instead)
          RxDOSMBR    (dropped, use ldosmbr instead)

     4. Format of the source files modified:

         1. The source files now use tab characters to make editing them
            easier. The tab characters should be expanded to up to 8 spaces
            for displaying.

         2. Removed the funny boxes drawn with ; ' - . characters.

     5. Replaced COPYING and gpl.txt by LICENSE.TXT which is an up-to-date
        plain-text GNU GPL Version 2 from http://www.gnu.org/

     6. Directory structure changed.

        Former:

          \         Binaries, documentation, license,
                     makefile, link files
          \sources  Actual source, include/macro files, license
          \objs     Object files
          \errors   Assembler and linker error messages

        New:

          \         Make scripts
          \RxBIO    RxBIO source
          \RxDOS    RxDOS source
          \RxCMD    RxCMD source
          \RxAPP    Sources of RxMBR, RxBOOT, RxD_BOOT, RxVDISK
          \MACRO    Macro files
          \doc      Documentation, license
          \lst      Listings
          \map      Map files
          \obj      Object files
          \bin      Binaries

     7. Some new or corrected code for RxDOS 7.30 added to source files. This
        code is deactivated using NASM conditional assembly and it is getting
        step by step through the next releases after RxDOS 7.20N.

8.2 Changes to DOS

8.2.1 New interfaces

     1. Int21.335E Get extended DOS version

          OUT: ax != 33FFh and ax != 3300h and ax != 0001h
               ax = 7852h "Rx" for RxDOS
               cx = build year
               dh = build month
               dl = build day
               bx = DOS kernel version
               di = BIO kernel version
                     (since RxDOS 7.23 always same as bx)
          	si = reserved, currently unset

     2. Int21.335F Get boot drive and filename

          INP:	dx = 0
          OUT: dx != 0 if supported
               dx:ax -> ASCIZ filename with drive
                         (file in root directory), in DOSDATA
          	cx, bx, di, si = reserved, currently unset

     3. Int21.3360 Get DOS capabilities

          INP:	CY
          OUT: NC, ax != 33FFh and ax != 3300h and ax != 0001h
               ax = DOS capability flags
               cx = reserved, set to zero
               dh = size of a DPB
               dl = reserved, set to zero
               bh = size of an SFT
               bl = size of a CDS entry
               di, si = reserved, currently unset

     4. Int21.33FF Get DOS version string (FreeDOS-compatible)

          INP: dx = 0
          OUT: dx != 0 if supported
               (may or may NOT clear the Carry Flag)
               dx:ax -> ASCIZ version string, in DOSCODE
          Example strings:
          "RxDOS version 7.24 [hg 2d2992dcc533+] [2018-04-06]"
          "FreeDOS kernel - SVN (build 2042 OEM:0xfd) [compiled Sep 22 2017]"

8.2.2 New kernel layout and building with linker obsolete

     1. Since version 7.21 (2017-09-01), both the RxBIO and RxDOS kernel parts
        can be built as flat binaries linked internally by NASM's "bin" output
        format. This means that no external linker is required any longer.

     2. Since version 7.22 (2018-03-09), the RxCMD shell can be built as flat
        binary linked internally by NASM's "bin" output format.

     3. As early as 1b22935d1222 2018-03-10, RxDOS.SYS is included by
        RxBIO.SYS's build process. Thus, a single-file kernel (similar to
        FreeDOS's) is first achieved this way.

     4. Since version 7.23 (2018-04-03) (as early as 7484396956e4 2018-03-30),
        the combined-kernel RxBIO.ASM is included into lDOS init. As early as
        bce5d6c1fbcd 2018-03-31, RxDOS.ASM is separately included into lDOS
        init, and RxDOS.SYS is no longer included by RxBIO.ASM.

     5. As early as 2a093d179ffd 2018-04-06, RxDOS.ASM is split into
        doscode.asm and dosdata.asm, and both are used respectively as DOSCODE
        and DOSDATA.

     6. As early as 3cb408e4a258 2018-04-12, RxBIO.ASM was split into
        biocode.asm and bioentry.asm, and the former is put into the DOSCODE
        section. Also, all variables except dosentry_to_dosdata_segment are
        moved out of DOSENTRY.

     7. As of b5b67355b503 2018-04-24, the interrupt 19h handler is left in
        DOSENTRY. This supports use cases where the HMA or UMA may not be
        available to the handler any longer.

8.2.3 Later kernel changes

     1. As of 8b75542e6ba1 2018-04-26, DOSCODE can be relocated to the HMA.

     2. As of af2c5e43be7e 2018-04-28, DEVICE and DEVICEHIGH directives in
        [RX]CONFIG.SYS are parsed in the same pass. Also, DEVICEHIGH correctly
        tries loading the device driver into the UMA if its executable image
        fits. This requires a two-step approach, and the allocation check in
        21.4B03 "load overlay" that was added in 58b36366e8e5 2018-05-03.

     3. As of e02d8abd452c 2018-05-03, lDOS memory.asm was implemented to
        replace RxDOSMEM (now all of the latter calls into lDOS).

     4. As of 282e01e8ec3e 2018-05-05, UMB linking during device driver
        processing (in init2_relocate_device) is implemented, and
        21.5802/.5803 (get/set UMB link state) is fully implemented.

     5. As of b758fe23b89b 2018-05-07, UMB linking and DOSDATA UMA relocation
        and DOSCODE UMA or HMA relocation are disabled by default; use UMB=ON
        and DOSDATA=UMA and DOSCODE=UMA,HMA in [RX]CONFIG.SYS to enable these.
        (The compatible option, DOS=UMB, means UMB=ON, while DOS=HMA or
        DOS=HIGH means DOSCODE=HMA. DOSDATA=UMA and DOSCODE=UMA do not have
        any equivalent compatible options.)

     6. As of 93ca7200fbc0 2018-05-12, UMA and HMA relocation can be turned on
        with UMA=ALL and HMA=ALL in [RX]CONFIG.SYS.

     7. As of bc8dee4269a0 2018-05-15, DR-DOS-style RPLOADER broadcasts are
        made.

     8. As of a9f4fa3bad20 2018-06-06, [RX]CONFIG.SYS is read using the loaded
        DOS like a normal user, not with disk accesses directly.

8.2.4 Misc

     1. RxDOS and RxCMD now allow embedding the Source Control Revision ID to
        identify the source used to build them. mak.sh needs to be called with
        use_build_revision_id=1 to enable this. It currently hardcodes the
        command to get the ID as "hg id -i". (b429250a515e 2018-03-15)

     2. RxCMD's VER command now includes support for the /R option, which in
        addition to the RxCMD version, Source Control Revision ID, build date,
        and copyright lines, also displays the reported DOS version and OEM
        code (Int21.3000), reported true DOS version (Int21.3306), and version
        string (Int21.33FF). (1be3f3e781ca 2018-03-14)

     3. RxDOSINI's AllocateSystemSpace is now implemented atop Int21.48,
        and creates S MCBs (owner = 8, name = "S", type stored later in
        name field). This enables MCB walkers to locate all allocations.
        (ac38e4a7eba1 2018-03-17)

     4. ldosboot and lmacros (b06c90a1009c 2018-03-28) are included as
        subrepos. ldos is included as subrepo. (8fa5fccbcf40 2018-03-30)

     5. instsect and iniblz are included as subrepos. (cd662caf7879 2018-10-
        08)
