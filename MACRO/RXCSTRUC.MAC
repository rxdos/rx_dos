[list -]
%if 0

RXCSTRUC - RxCMD data structures and definitions

This product is distributed AS IS and contains no warranty
whatsoever, including warranty of merchantability or
fitness for a particular purpose.

(c) Copyright 1990, 1999. Mike Podanoffsky

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, see
the file LICENSE.TXT.

%endif

%ifnnum __RXCSTRUC_MAC__
%assign __RXCSTRUC_MAC__ 1


		; Switches
	struc SWITCHENTRY
swChar			resb 1
swFlags			resw 1
swMinValue		resw 1
swMaxValue		resw 1
swActualValue		resw 1
	endstruc


SW_MINMAXVALUE		equ 0001h
SW_LETTERCHOICE		equ 0002h
SW_SWITCHSET		equ 8000h
sizeSWITCHENTRY		equ SWITCHENTRY_size


		; SPRINTF function flags
SPRINTF_SEPARATOR	equ 8000h
SPRINTF_LONG		equ 4000h
SPRINTF_LEFTALIGN	equ 2000h
SPRINTF_ZEROFILL	equ 1000h
SPRINTF_FAR		equ 0800h
SPRINTF_IMMEDIATE	equ 0400h

SPRINTF_COMMADELIM	equ SPRINTF_SEPARATOR		; obsolete. do not use
SPRINTF_LONGFLAG	equ SPRINTF_LONG		; obsolete. do not use
SPRINTF_ZEROFIELDFILL	equ SPRINTF_ZEROFILL		; obsolete. do not use


		; Translate macro
		;
		; %1 = which value required in al
		; %2 = which value to place in ah (RxDOS 7.30: al)
		; %3 = where to jump to if match (RxDOS 7.20N only)
%if 1
	%imacro translate 3.nolist
		mov ah, %2
		cmp al, %1
		je %3
	%endmacro
%else
	%imacro translate 2.nolist
		cmp al, %1
		jne %%notequal
		mov al, %2
%%notequal:
	%endmacro
%endif


		; Define date/time template
	%imacro intldate 2.nolist
		dw %1
		db %2,0
	%endmacro


		; Extended Open
	%imacro exopenfile 1-2.nolist OPEN_ACCESS_READWRITE	; defaults to read/write mode
		mov bx,%2
%ifnidni %1,si
		mov si,%1
		; RxDOS 7.20N:	No need to compare to registers because
		;		NASM doesn't need the "offset" keyword.
%endif
		xor cx,cx
		mov dx,EXTENDEDACTION_OPEN
		int21 LFNExtendedOpenCreate		; try to open
	%endmacro


		; Extended Create
	%imacro excreatefile 1-2.nolist ATTR_NORMAL		; defaults to all attributes cleared
		mov cx,%2
%ifnidni %1,si
		mov si,%1
		; RxDOS 7.20N:	No need to compare to registers because
		;		NASM doesn't need the "offset" keyword.
%endif
		mov dx, (EXTENDEDACTION_CREATE | EXTENDEDACTION_TRUNCATE)
		int21 LFNExtendedOpenCreate		; try to create
	%endmacro


		; File Attributes
	%imacro fileattrib 0-2.nolist
%if %0 == 0
		xor cx, cx
%else
 %if %1 == ATTR_NORMAL		; 0
		xor cx, cx
 %else
		mov ch, %1
 %endif
 %if %0 == 1
		mov cl, ch
 %else
		mov cl, %2
 %endif
%endif
	%endmacro


		; FOR argument list
	struc FORARGS
forVarIdent		resw 2
forVarLetter		resw 2
forInArg		resw 2
forInStartParen		resw 2
	endstruc

_IN			equ 0
_DO			equ 1


		; Date control block
	struc DATE
_DAY			resw 1
_MONTH			resw 1
_YEAR			resw 1
	endstruc


		; Time control block
	struc TIME
_HOURS			resw 1
_MINUTES		resw 1
_SECONDS		resw 1
_HUNDREDTHS		resw 1
	endstruc

sizeDATE		equ DATE_size
sizeTIME		equ TIME_size


		; Argument table entry
	struc CMDARGUMENT
argpointer		resw 1			; argument pointer
arglength		resw 1			; argument length
	endstruc


maxArgArray		equ 64 
sizeArgCmd		equ CMDARGUMENT_size
sizemaxArgArray		equ ( maxArgArray * sizeArgCmd )


		; Parsed Filename Struct
	struc PARSEDFILENAME
parsedrive		resw 1			; drive value
parsewildcharflag	resw 1			; wild char flag
parsestringlength	resw 1			; length of string
parsepathlength		resw 1			; length of drive/path string
parsefilenamelength	resw 1			; length of filename string
parseextensionlength	resw 1			; length of extension string

parseptrstartstring	resd 1			; start of string
parseptrfilename	resw 1			; ptr to filename (after last \)
parseptrextension	resw 1			; ptr to extension (after .)
	endstruc

sizeParsedFilename	equ PARSEDFILENAME_size
SAFE_ALLOCATION		equ ( 8 * 1024)


		; Batch arguments
	struc BATCH_ARGS
batchArgPtrs			resw 1			; 0
				resw 1			; 1 arg pointers
				resw 1			; 2	.
				resw 1			; 3	.
				resw 1			; 4	.
				resw 1			; 5	.
				resw 1			; 6	.
				resw 1			; 7	.
				resw 1			; 8	.
				resw 1			; 9	.

batchNumArgs			resw 1			; number of batch arguments
batchFileHandle			resw 1			; batch file handle
batchFilePosition		resd 1			; batch file position
batchEchoStatus			resw 1			; echo status

batchArgStore			resb 1024
	endstruc

sizeBATCH_ARGS			equ BATCH_ARGS_size


		; Execution Control
	struc EXECCONTROL
exCtrlArgArray			resd 1
exCtrlStdInHandle		resw 1
exCtrlStdOutHandle		resw 1

exCtrlFlags			resw 1
exCtrlStdInFileName		resb sizeEXPANDNAME
exCtrlStdOutFileName		resb sizeEXPANDNAME
exCtrlExecBlock			resb sizeEXEC
	endstruc

sizeEXECCONTROL			equ EXECCONTROL_size


		; EXECCONTROL flags
exCtrlPiped			equ 8000h
exCtrlAppend			equ 4000h		; append to StdOut
exTempInputFile			equ 2000h		; temp StdIn created


_BAT				equ 0
_EXE				equ 1
_COM				equ 2
_UNKNOWN			equ -1


		; IF special values
IF_NOT				equ 01h
IF_ERRORLEVEL			equ 02h
IF_EXIST			equ 03h


_ARGTEXT			equ _high
_ARGTYPE			equ _low


%endif ; __RXCSTRUC_MAC__

[list +]
