[list -]
%if 0

RxDMACRO - RxDOS & DOS macro definitions

This product is distributed AS IS and contains no warranty
whatsoever, including warranty of merchantability or
fitness for a particular purpose.

Copyright (C) 1990-1999 Mike Podanoffsky

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, see
the file LICENSE.TXT.

%endif

%ifndef __RXDMACRO_MAC__
%assign __RXDMACRO_MAC__ 1


%ifdef __lMACROS1_MAC__
 %fatal "lmacros1.mac already included"
%endif
%ifdef __lMACROS2_MAC__
 %fatal "lmacros2.mac already included"
%endif
%ifdef __RXOPTION_MAC__
 %fatal "RXOPTION.MAC or replacement already included"
%endif

%include "lmacros2.mac"
[list -]
%include "RXOPTION.MAC"
[list -]


		; Compare and goto (short jump)
		;
		; %1 = Compare this value to al or ax
		; %2 = Jump short to this label if value matches
		;
		; Hint:	If you want to compare a value <= 0FFh to ax,
		;	write "10000h|value" (or just the value + 10000h)
	%imacro gotos 2.nolist
%if %1 > 0FFh
		cmp ax, %1 & 0FFFFh
%else
		cmp al, %1 & 0FFh
%endif
		je %2
	%endmacro


		; Specify device header
		;
		; %1 = Label to place in front of the header
		; %2 = Pointer of the next driver
		; %3 = Segment of the next driver
		; %4 = Device attributes
		; %5 = Pointer to the driver's strategy code
		; %6 = Pointer to the driver's interrupt code
		; %7 = For character devices, device name (gets filled to 8 with blanks)
		;      For block devices, number of supported units
		; %8 = For block devices, device name (gets filled to 7 with blanks)
		; %8 = for character devices, word data to append
		; %9 = word data to append (greedy parameter)
	%macro devheader 7-9+.nolist
%1:		dw %2,%3,%4,%5,%6	; Addresses and attribute

%if %4 & 8000h				; If it's a character device
		fill 8,32,db %7		;  then store name filling to 8
 %if %0 >= 8				; if 8th parameter given
		dw %8
  %if %0 >= 9
		dw %9
  %endif
 %endif
%else					; Else it's a block device
		db %7			;  then store supported units
 %if %0 < 8
  %error "devheader: Missing 8th parameter. Expected name for block device"
 %endif
 %ifnidn %8,-
		fill 7,32,db %8		; and store name filling to 7
 %endif
 %if %0 >= 9
		dw %9
 %endif
%endif
	%endmacro


		; Specify entry for list of saved interrupts
		;
		; %1 = Interrupt number, -1 or FFh indicates end of list
		; %2 = Pointer of the saved interrupt (optional, default 0)
		; %3 = Segment of the saved interrupt (optional, default 0)
	%macro savint 1-3.nolist 0,0
		db %1			; Interrupt number
		; RxDOS 7.20N:	MS-DOS saves the interrupts using one byte per number
		;		and four bytes for the actual address. Unfortunately,
		;		some software relies on this table layout.
%if %1 < 0FFh && %1 >= 0		; If it's not the end of list marker
		dw %2, %3		;  then store the saved interrupt address
%endif
	%endmacro


		; Get one parameter out of some
		;
		; %1 = Number of parameter to retrieve, 1 = %2, 2 = %3, etc.
		; %2+ = Parameters
		;
		; %defines __oneparam to retrieved parameter
	%imacro getoneparam 1-*.nolist
%rotate %1
%define __oneparam %1
	%endmacro
		

%if 0
		; Test some %ifidni conditions
		;
		; %1 = Source string
		; %2 = One or more strings for comparison
		;
		; %assigns %$idni to 1 if one comparison matched, else to 0
		; %assigns %$idnicount to the number of the matching string (starts numbering with 1)
	%imacro ifidni 2-*.nolist
%push ifidni
%assign %$idni 0			; assume not found
%assign %$idnicount 0
%define %$source %1			; must store source elsewhere because %rotates through parameters
	%rep %0 - 1			; compare all except initial %1
%rotate 1				; done before the first comparison, initial %1 never compared
%assign %$idnicount %$idnicount+1	; done before the first comparison, first string is 1
%ifidni %$source,%1			; actual comparison
%assign %$idni 1
	%exitrep			; no need to try more
%endif
	%endrep
	%if 1	; NASM bug
%assign __ifidni_idnicount %$idnicount
%assign __ifidni_idni %$idni
%pop
%assign %$idnicount __ifidni_idnicount
%assign %$idni __ifidni_idni
%undef __ifidni_idnicount
%undef __ifidni_idni
	%else	; NASM bug
		; RxDOS 7.20N:	Another NASM bug. Somehow below preprocessor lines don't work.
		; Note:		%$$ seems to be intended for labels. Possibly this is the reason
		;		it doesn't work well to access single-line macros of the context.
%assign %$$idnicount %$idnicount; %$$ means to use former NASM context
%assign %$$idni %$idni		; %$$ means to use former NASM context
%pop
	%endif	; NASM bug
	%endmacro

%else ;0
		; Test some %ifidni conditions
		;
		; %1 = Source string
		; %2 = One or more strings for comparison
		;
		; %assigns __idni to 1 if one comparison matched, else to 0
		; %assigns __idnicount to the number of the matching string (starts numbering with 1)
	%imacro ifidni 2-*.nolist
%push ifidni
%assign __idni 0			; assume not found
%assign __idnicount 0
%define %$source %1			; must store source elsewhere because %rotates through parameters
	%rep %0 - 1			; compare all except initial %1
%rotate 1				; done before the first comparison, initial %1 never compared
%assign __idnicount __idnicount+1	; done before the first comparison, first string is 1
%ifidni %$source,%1			; actual comparison
%assign __idni 1
	%exitrep			; no need to try more
%endif
	%endrep
%pop
	%endmacro
%endif ;1

		; Some comparison strings for above ifidni macro
%idefine _segregs	cs,ds,es,ss,fs,gs
%idefine _8regs		al,ah,bl,bh,cl,ch,dl,dh
%idefine _16regs	ax,bx,cx,dx,di,si,bp,sp
%idefine _32regs	eax,ebx,ecx,edx,edi,esi,ebp,esp
%idefine _16segregs	_16regs,_segregs
%idefine _regs		_8regs,_16regs,_32regs,_segregs
%idefine _32oregs	fd,eip
%idefine _16oregs	f,fw,ip

		; RxDOS 7.20N:	Macros translated from RxDOS 7.1.5


		; Macros
%iassign zero 0
%iassign one 1
%iassign two 2
%iassign minusone -1
%iassign null 0

%iassign no 0
%iassign yes 1

%iassign off 0
%iassign on 1

%iassign _off 0
%iassign _on 1

%iassign false 0
%iassign true 1

%iassign __pointer 0
%iassign __segment 2

%iassign _pointer 0
%iassign _segment 2

%iassign _low 0
%iassign _high 2

%iassign _q0 0
%iassign _q1 2
%iassign _q2 4
%iassign _q3 6

%iassign paragraph 16

%iassign quote		"'"
%iassign singlequote	"'"
%iassign doublequote	'"'
%iassign openbracket	'['
%iassign closebracket	']'

%iassign comma		','
%iassign period		'.'
%iassign colon		':'
%iassign semicolon	';'
%iassign asterisk	'*'
%iassign questionmark	'?'
%idefine ctrl(x)	((x) - 40h)
%iassign controlc	ctrl('C')
%iassign controli	ctrl('I')
%iassign controlj	ctrl('J')
%iassign controlm	ctrl('M')
%iassign controlq	ctrl('Q')
%iassign controls	ctrl('S')
%iassign controlz	ctrl('Z')

%iassign bell		7
%iassign bs		8
%iassign tab		9
%iassign lf		10
%iassign cr		13
%iassign eof		26
%iassign esc		27


		; Caller's stack frame for RxDOS's Int21 dispatcher
%iassign _fl		6
%iassign _cs		4
%iassign _ip		2
%iassign _es		0
%iassign _ds		-2
%iassign _bp		-4
%iassign _di		-6
%iassign _si		-8
%iassign _dx		-10
%iassign _cx		-12
%iassign _bx		-14
%iassign _ax		-16
%iassign sizestackframe	16		; -(_ax)

%iassign _flags			_fl
%iassign _extrasegment		_es	; obsolete, use the short form
%iassign _datasegment		_ds	; obsolete, use the short form
%iassign rxdosuserstackpointer	0


		; Register definitions for high/low bytes
%iassign _al		0
%iassign _ah		1
%iassign _bl		0
%iassign _bh		1
%iassign _cl		0
%iassign _ch		1
%iassign _dl		0
%iassign _dh		1
%iassign _sil		0
%iassign _sih		1
%iassign _dil		0
%iassign _dih		1


		; Colours
%iassign black		0
%iassign blue		1
%iassign green		2
%iassign cyan		3
%iassign magenta	5
%iassign red		6
%iassign gray		7		; white
%iassign grey		gray

%iassign lightblack	8		; Dark grey
					; RxDOS 7.20N: Older releases accidently assigned lightblack to 9 as well
%iassign darkgray	lightblack
%iassign darkgrey	darkgray
%iassign lightblue	9
%iassign lightgreen	10
%iassign lightcyan	11
%iassign lightred	12
%iassign lightmagenta	13
%iassign yellow		14
%iassign white		15		; light white

%iassign foreground	black		; Default colors of the DOS prompt
%iassign background	grey


		; Colour Define
	%imacro colour 1-2.nolist 0
		db (%2 << 4) + %1
	%endmacro

%idefine color colour


		; Get Double Argument
	%imacro getdarg 3.nolist
%push
	ifidni %1,_segregs
%if __idni
		l%1 %2, [bp+%3]
%else
		mov %2, word [bp+_pointer+%3]
		mov %1, word [bp+_segment+%3]
%endif
%pop
	%endmacro


		; Get Argument
	%macro getarg 2.nolist
		mov %1, word [bp+%2]
	%endmacro


		; Store Double Argument
	%macro stordarg 3.nolist
		mov word [bp+_pointer+%1], %3
		mov word [bp+_segment+%1], %2
	%endmacro


		; Store Argument
	%macro storarg 2.nolist
		mov word [bp+%1], %2
	%endmacro


%idefine fl f				; DEBUG calls the flags FL instead

		; Internal Push
	%macro push 1.nolist
%ifidni %1,-
			; Nothing
%elifidni %1,ip
		o16
		call %%next
%%next:
%elifidni %1,eip
		o32
		call %%next
%%next:
%elifidni %1,f
		pushf
%elifidni %1,fw
		pushfw
%elifidni %1,fd
		pushfd
%else
		%? %1
%endif
	%endmacro

		; Internal Pop
	%macro pop 1.nolist
%ifidni %1,-
			; Nothing
%elifidni %1,ip
		o16 retn
%elifidni %1,eip
		o32 retn
%elifidni %1,f
		popf
%elifidni %1,fw
		popfw
%elifidni %1,fd
		popfd
%else
		%? %1
%endif
	%endmacro

%idefine tpush push
%idefine tpop pop


		; Save Registers
	%imacro  mpush 0-*.nolist
	%rep %0
		tpush %1
%rotate 1
	%endrep
	%endmacro

		; Restore Registers
	%imacro  mpop 0-*.nolist
	%rep %0
		tpop %1
%rotate 1				; Not NASM manual multipop!
	%endrep
	%endmacro
	
%idefine saveregisters mpush
%idefine restoreregisters mpop


		; Save All Registers
	%imacro saveallregisters 0.nolist
		mpush es, ds, bp, di, si, dx, cx, bx, ax
	%endmacro


		; Restore All Registers
	%imacro restoreallregisters 0.nolist
		mpop ax, bx, cx, dx, si, di, bp, ds, es
	%endmacro


		; Save Segments and Registers
	%imacro savesegments 0-1+.nolist -
		mpush ds, es, %1
	%endmacro


		; Restore Segments and Registers
	%imacro restoresegments 0-1+.nolist -
		mpop %1, es, ds
	%endmacro


		; Current stack segment
	%imacro currsegment 0-*.nolist
	%rep %0
%rotate 1
		push ss
		pop %1 
	%endrep
	%endmacro


		; Standard Save
	%imacro savestandard 0-1+.nolist -
		mpush ds, es, si, di, %1
	%endmacro


		; Standard Restore
	%imacro restorestandard 0-1+.nolist -
		mpop %1, di, si, es, ds
	%endmacro

		; Set Segment Register
	%imacro setseg 2.nolist
%push
	ifidni %2,_16regs
%if __idni
		mov %1, %2
%else
		push word %2
		pop %1
%endif
%pop
	%endmacro

%idefine setds setseg ds,
%idefine setes setseg es,
%idefine setss setseg ss,
%idefine setfs setseg fs,
%idefine setgs setseg gs,


		; Adds an offset to a seg register pair
	%imacro offsetaddr 3.nolist
	ifidni %1,_segregs
		add %2, %3
		jnc %%no_overflow
%if __idni
		push ax
		mov ax, %1
		add ax, 1000h		; account for carry
		mov %1, ax
		pop ax
%else
		add %1, 1000h
%endif
%%no_overflow:
	%endmacro


		; Defines a specialised entry
		;
		; Doesn't alter registers and doesn't push bp
		;
		; Don't use return macro there's no stack frame!
		;
		; %1 = number of words from bp to last argument (above ip/bp)
		; %2 = __retentry (how many words return should remove)
		; %3 = entry type (0, 4, 6, 8)
	%imacro _specialentry 1-3.nolist 0,0
%assign __defentry 0
%assign __retentry %2
%assign __lastarg 2*%1
; %assign __maxargs  2*__retentry
; %assign __argentry __maxargs+(2*%1)
%assign __argentry (2*__retentry)+(2*%1)
%assign __returnargentry __argentry
%assign __entrytype %3			; RxDOS 7.20N:	Added to tell return to use either retn or retf.
	%endmacro


		; Defines a standard entry
		;
		; Pushs bp and sets bp to value of sp after pushing.
		;
		; Use return macro to return.
		;
		; %1 = 2*%1 for _specialentry, __entrytype (size of return address + 2 for stacked bp)
		; %2 = %2 for _specialentry
	%imacro _entry 1-2.nolist 0	; %1 = # bytes for return address + 2,  %2 = # word entries
		push bp
		mov bp, sp
 %ifempty %2
		_specialentry %1/2,0,%1
 %else
		_specialentry %1/2,%2,%1
 %endif
%assign __callargs 4			; RxDOS 7.20N:	Possible intended to be used like __entrytype. However,
					;		no code uses __callargs and farentry doesn't set it.
	%endmacro


		; %1 of specialentry is in fact %2 of _specialentry
; %idefine specialentry _specialentry 3,	; far
	%imacro specialentry 0-1+.nolist
%warning specialentry used
		_specialentry 3,%1
	%endmacro

		; %1 of entry/farentry/intentry is in fact %2 of _entry
%idefine entry _entry 4,		; near
%idefine farentry _entry 6,		; far
%idefine intentry _entry 8,		; far + flags


		; Defines a call parameter argument
	%imacro _arg 2.nolist		; %1 = argument size in byte, %2 = argument name
; %if __maxargs < %1
;  %error "_arg definition does not match _entry"
; %endif
	; This check doesn't really make sense.

%assign __argentry __argentry-%1
%iassign _%2 __argentry

%if __argentry < __lastarg
 %error "_arg definition does not match _entry"
%endif
	%endmacro

%idefine arg _arg 2,			; word
%idefine darg _arg 4,			; dword


		; Argument references (Args pushed after call, but before entry)
	%imacro _rarg 2.nolist
%iassign _%1 %2
	%endmacro

%idefine rarg _rarg
%idefine rdarg _rarg


		; Define a call parameter argument which is used to return a parameter
	%imacro _returnarg 2.nolist	; %1 = argument size in byte, %2 = argument name
%iassign _%2 __returnargentry
%assign __returnargentry __returnargentry+%1
	%endmacro

%idefine returnarg _returnarg 2,	; word
%idefine returndarg _returnarg 4,	; dword
%idefine returnqarg _returnarg 8,	; dword


		; Defines a word argument on stack
	%imacro def 1-2.nolist
%assign __defentry __defentry-2
%iassign _%1 __defentry

%push
%assign %$pushedreg 0

%ifn %0 >= 2
		push ax
%assign %$pushedreg 1

%else ; %0 >= 2
	ifidni %2,_16segregs,_16oregs
%if __idni
		push %2
%assign %$pushedreg 1
%endif ; __idni

%endif ; %0 >= 2

%ifn %$pushedreg
		push ax
		mov word [bp+_%1], %2		; really a value
%endif
%pop
	%endmacro


		; Defines a double word argument on stack
	%imacro ddef 1-3.nolist
%assign __defentry __defentry-4
%iassign _%1 __defentry

%push
%assign %$pushedreg 0
%assign %$pushedseg 0

%if %0 >= 2
	ifidni %2,_16segregs,_16oregs
 %if __idni
		push %2
  %assign %$pushedseg 1
 %endif ; __idni
%endif ; %0 >= 2

%if %$pushedseg == 0
		push ax
 %if %0 >= 2
		mov word [bp+_segment+_%1], %2	; really a value
 %endif
%endif

%if %0 >= 3
	ifidni %3,_16segregs,_16oregs
 %if __idni
		push %3
  %assign %$pushedreg 1
 %endif ; __idni
%endif ; %0 >= 3

%if %$pushedreg == 0
		push ax
 %if %0 >= 3
		mov word [bp+_pointer+_%1], %3	; really a value
 %endif
%endif
%pop
	%endmacro


		; Defines n word arguments on stack
	%imacro defwords 2.nolist
%push
%assign %$tempsize 2*%2
%assign __defentry __defentry-%$tempsize
%iassign _%1 __defentry
%iassign sizeof_%1 %$tempsize

%if (%$tempsize < 128)
		sub sp,byte %$tempsize
%else
		sub sp,%$tempsize
%endif
%pop
	%endmacro


		; Defines n byte arguments on stack (ends on even boundry)
	%imacro defbytes 2.nolist
%push
%assign %$tempsize (%2+1)/2*2
%assign __defentry __defentry-%$tempsize
%iassign _%1 __defentry
%iassign sizeof_%1 %2

%if (%$tempsize < 128)
		sub sp,byte %$tempsize
%else
		sub sp,%$tempsize
%endif
%pop
	%endmacro


		; Creates an argument definition but you must do the push
		;
		; %1 = Number of bytes argument takes up (expects even number)
		; %2 = Name of label (prefixed with underscore)
	%imacro _temp 2.nolist
		checkeven %1
%assign __defentry __defentry-%1
%iassign _%2 __defentry
	%endmacro

%idefine temp _temp 2,
%idefine dtemp _temp 4,
%idefine qtemp _temp 8,


		; Returns from _entry
		;
		; %1 = Optional stack adjustment in bytes (done after popping bp)
		; %2 = If false, use 12 byte stack adjustment code that won't change flags.
		;      If true, use 4 byte add instruction that changes flags.
		;      If %1 equals zero, %2 doesn't change anything.
		; %3 = If false, adjust stack according to __defentry or by restoring sp from bp.
		;      If true, don't adjust stack before popping bp.
		;
		; RxDOS 7.20N:	bp is moved into sp, then the stacked bp is restored.
	%imacro return 0-3.nolist 0,0,0
		checkeven __defentry
		checkeven __entrytype
		checkeven %1
%ifn %3
		mov sp, bp
%endif
		pop bp
%if %1 < 0
 %error "return: stack adjustment mustn't be negative"
%elif %1
%if %2
		add sp, %1
%else
		push bp
		mov bp, sp			; create stack frame
		lea bp, [bp+2+%1]		; compute new sp
		xchg bp, word [bp-2-%1]		; restore bp and store new sp
		pop sp				; increments sp, then gets the new value
%endif
%endif

%if __entrytype == 0		; no return (__specialentry)
%elif __entrytype == 4		; near return
 %if __retentry
		retn 2 * __retentry
 %else
		retn
 %endif
%elif __entrytype == 6		; far return
 %if __retentry
		retf 2 * __retentry
 %else
		retf
 %endif
%elif __entrytype == 8		; far + flags return
 %if __retentry
  %error "return: _entry for interrupts can't adjust stack after returning"
 %else
		iret
 %endif
%else
 %error "return: Unknown __entrytype"
%endif
	%endmacro


		; seterror
		;
		; sets carry, loads an error value into ax,
		; optionally jumps to an address.
		;
		; %1 = error value to load into ax (byte override accepted)
		; %2 = optional unconditional jump address (short override accepted)
	%imacro seterror 1-2.nolist
		stc
		mov ax, %1
%if %0 >= 2
		jmp %2
%endif
	%endmacro


		; iferror
		; if CY: load an error value into ax, jump to an address.
	%macro iferror 2.nolist
		jnc %%noerror
		mov ax, %1
		jmp %2
%%noerror:
	%endmacro


		; Compare and Goto
		;
		; %1 = Value to compare to ax (> 0FFh) or al (<= 0FFh)
		; %2 = Jump address to branch to if equal (ZR)
	%imacro goto 2.nolist
%if %1 > 0FFh
		cmp ax,%1 & 0FFFFh
%else
		cmp al,%1 & 0FFh
%endif

		ife %2
	%endmacro


%idefine ifc	jc
%idefine ifnc	jnc
%idefine ifz	jz
%idefine ifnz	jnz
%idefine ife	je
%idefine ifne	jne


		; Normalize segment and pointer
		; Makes sure pointer is in range 00h-0Fh, adds higher values to segment
		;
		; %1 = Register that contains segment
		; %2 = Register that contains pointer
	%imacro normalizebuffer 2.nolist
		test %2, 0FFF0h
		jz %%isnormalized		; no need to adjust -->
		push %2				; remember low nibble value
%ifn _186
		shr %2, 1
		shr %2, 1
		shr %2, 1
		shr %2, 1			; get value of higher three nibbles into pointer
%else
		shr %2, 4			; get value of higher three nibbles into pointer
%endif
		push ax
		mov ax, %1
		add ax, %2			; add higher three nibbles to segment
		mov %1, ax
		pop ax
		pop %2				; restore low nibble value
		and %2, byte 000Fh		; mask out anything but low nibble
%%isnormalized:
	%endmacro


		; Clear memory starting at es:di (actually: fill with words)
		;
		; %1 = Size of memory area in byte (hyphen to use value in cx)
		; %2 = Optional word value to fill memory with (default 0) (hyphen to use value in ax)
	%imacro clearmemory 0-2.nolist -,0
		push di
%ifnidn %2,-
		push ax
%endif
		push cx
%ifnidn %1,-
		mov cx, %1
%endif
%ifnidn %2,-
%if %2 == 0
		xor ax, ax
%else
		mov ax, %2
%endif
%endif
		shr cx, 1
		repe stosw
		jnc %%evencount
		stosb
%%evencount:
		pop cx
%ifnidn %2,-
		pop ax
%endif
		pop di
	%endmacro


		; Fast Move
	%imacro fastmove 0.nolist
		shr cx,1			; get number of words
		repz movsw			; move them
		jnc %%evencount			; even bytes -->
		movsb				; move odd byte
%%evencount:
	%endmacro


		; Return caller's stack frame created by Int21 dispatcher
		; reg defined contains stack reference to the stack frame.
		; Use _ax, _bx etc. macros to address these registers
	%imacro retcallersstackframe 2.nolist
	%rep 2
%ifidni %1,ss
 %error "retcallersstackframe: changing ss would cause errors"
%endif
%rotate 1
	%endrep
%rotate -2
		call _RetCallersStackFrame
		pop %2
		pop %1
	%endmacro


		; Define Command
		;
		; %1 = word to place in data part of table entry
		; %2+ = command string
		;
		; Table entry layout:
		;  wo:00	data, or 0FFFFh terminator
		;  wo:02	size of table entry, including padding and data
		;     04	command string, zero terminated
		;  by:??	Padding zero byte to align to 2, if required
	%imacro cmnd 2+.nolist
%%start:
		dw %1
		dw %%end - %%start
		asciz %2
%if 1
		align 2,db 0
%endif
%%end:
	%endmacro


		; Define switches
		;
		; %1 = byte switch letter
		; %2, %3, %4, %5 = word data
		;
		; Table entry layout:
		;  by:00	switch letter, or 0FFh terminator
		;  wo:02	%2
		;  wo:04	%3
		;  wo:06	%4
		;  wo:08	%5
	%imacro switch 1-5.nolist 0, 0, 0, 0
		db %1
		dw %2,%3,%4,%5
	%endmacro


		; Uppercase
	%imacro _uppercase 0-1.nolist al
		cmp %1, 'a'
		jb %%noletter
		cmp %1, 'z'+1
		jae %%noletter
		and %1, ~20h
%%noletter:
	%endmacro


		; Lowercase
	%imacro _lowercase 0-1.nolist al
		cmp %1, 'A'
		jb %%noletter
		cmp %1, 'Z'+1
		jae %%noletter
		or %1, 20h
%%noletter:
	%endmacro


	%imacro ifs 0-1.nolist
%if %0 >= 1
		mov ax, 1100h|%1
%else
		mov ah, 11h
%endif
		int 2Fh
	%endmacro

	%imacro intr 1-3.nolist
%if %0 >= 2
 %if %0 >= 3
  %if (%2 > 0FFh)
		mov ax,%2|%3
  %else
		mov ax,%2<<8|%3
  %endif
 %else
  %if (%2 > 0FFh)
		mov ax,%2
  %else
		mov ah,%2
  %endif
 %endif
%endif
		int %1
	%endmacro

%idefine int21 intr 21h,
%idefine int2e intr 2Eh,


		; Extended interrupt (interrupt can change bp)
		;
		; RxDOS 7.20N:	Huh, why is it called "Extended"?
	%imacro exint 1.nolist
		push bp
		int %1
		pop bp
	%endmacro


%endif ; __RXDMACRO_MAC__

[list +]
